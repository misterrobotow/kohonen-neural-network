# Entrenamiento de la red neuronal de kohonen
# Autor: Francisco Daniel Gomez Alvarez

from io import open
from kohonenBase import Kohonen
import sys

def createKohonen():
    file = open("wValues.txt","r")
    lines = file.readlines()
    nm = lines[0].split();
    end = len(lines)
    wValues = []
    for i in range(1,end):
        strValues = lines[i].split()
        wValues.append([float(y) for y in strValues])
    file.close()
    ko = Kohonen(int(nm[0]),int(nm[1]))
    ko.setW(wValues)
    return ko


if len(sys.argv) == 3:
    inStr = sys.argv[2].split()
    if sys.argv[1] == 'int':
        Ek = [int(x) for x in inStr]
    else:
        Ek = [float(x) for x in inStr]
    print("Patron de entrada: "+ str(Ek))
    kohonen = createKohonen()
    distances = kohonen.distance(Ek)
    print("Salida: Se parece al numero: " + str(distances[0][1]))

else:
    print("Argument error!")

